package com.edi.prod;

import com.edi.common.MsSqlConnection.MsSqlConnection;
import com.edi.common.MySqlConnection.MySqlConnection;
import com.edi.common.utils.GetDbConnDetail;
import java.awt.Cursor;
import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import javax.swing.*;

public class Main extends javax.swing.JFrame {

    private static final long serialVersionUID = 1L;
// general vars.
    private GetDbConnDetail GetConDetail = new GetDbConnDetail();
    private MsSqlConnection mssql = null;
    private MySqlConnection mysql = null;
    private boolean mys = false;
    private boolean mss = false;
    private Connection conn = null;
    private Statement stmt = null;
    private Statement stmt2 = null;
    private ResultSet rf = null;
    private boolean bLoadFile = false;
    private String sLoadFile = null;
    private boolean bAutoRun = false;
    private boolean bAutoManRun = false;
    private boolean bWriteOutput = true;
// Tag vars
    private String sFieldSeparator = "\t";
    private String sArchivePath = null;
    private boolean bArchive = false;
    private boolean bForceTime = false;
    private boolean bNoOutput = false;
    private boolean bRemoveTime = false;
    private boolean bShowNulls = false;
    private boolean bFixedWidth = false;
    private boolean bgotFilename = false;
    private String slineFormat = "\r\n";
    private String seq = "";
    private String[] rstab = null;
    private String[] sFixedWidthOffsets;
    private JFileChooser jfc;
//to get first use database name
    private String topdb = null;
    private String filepath = "";
    private String FileNamePrefix = "";
    private String filename = "";
    private String Suffix = "";
    private String fileExt = "";
    private String ParaFileNamePrefix = null;
    private String ParaFileName = null;
    private String ParaSuffix = null;
    private String ParafileExt = null;
    private String ParafileHeaderText = null;
    private String ParafileHeaderExt = null;
    private String ParafileFooterText = null;
    private String DtFormat = null;
    private String TmFormat = "";
    private String sVersion = "1.1";
    private String sAppName = "Production";
    private String tmpString = null;
    private String fldDate = null;
    private String sToday = null;
    private String driver = null;
    private String CntField = null;
    private String RecCnt = "";
    private String Notify = "";
    private String sFileNamePrefix = "";
    private String sFileExt = "";
    private boolean sFileExists = true;
    private String sFieldHeaders = "false";
    private String AppPath = System.getProperty("user.dir");
    private String replaceFrom = "";
// Tag Names and Parameters.
// The following to vars are used to parse the tags, the First is the tag name
// the Second is the tag default value and the Third is the queries found withing
// in the order found the script.
    private String[] Tags = new String[]{
        "--NOOUTPUT=",
        "--FILENAMEPREFIX=", //Prefix of the file
        "--FILENAME=", //File Date Format
        "--FILENAMEALT=", //Altering file name by passing query from script
        "--FILEEXTENSION=", //Extension of the file
        "--SUFFIX=", //Suffix of the file
        "--FILEHEADERTEXT=", //Header of the file
        "--FILEHEADERDATE=", //Set date in the file header
        "--DATADATEFORMAT=", //Set the date format in file
        "--FORCETIME=", //Flag for set the time in the date and time field
        "--FILEFOOTERTEXT=", //Set the footer text in the file
        "--FIELDSEPARATOR=", //Delimiter for the column in the file
        "--OUTPUTSTYLE=", //Set output into different format for e.g. FixedWidth
        "--ARCHIVE=", //Flag for archiving file
        "--ARCHIVEPATH=", //Set archive path for file
        "--FIELDHEADERS=", //Flag for set the field header in file
        "--FILETIDY=", //Delete files from the folder
        "--FWOFFSETS=", //Set offsets for FixedWidth file
        "--INCREMENTAL=", //Flag for incremental file
        "--SEVENT=", //
        "--SHOWNULLS=", //Flag to set the 'null' where fieldvalue is null in output file
        "--REMOVESTRIKE=", //Flag to remove HTML Tag '<strike>.*?</strike>' from the output feed
        "--STARTCOMMENT=", //Delete anything between startcommen and endcomment
        "--ENDCOMMENT=", //Delete anything between startcommen and endcomment
        "--REMOVEHTML=", //Flag to remove HTML Tags from the output feed
        "--HARDLINEBREAKS=", //Flag to replace line break '<br />' to '\r\n'
        "--ENABLEORDINAL=", //Flag to replace ordinal values of ASCII characters to printable character or to nothing
        "--ENABLEPUNCTUATION=", //Flag to replace all the punctuation marks. For e.g. "&lt;" to "<"
        "--COUNTERFIELD=", //Set the record count field in the output feed (which is pass by script)
        "--REPLACECHAR=", //Replace characters (character set pass by script to program)
        "--RECCOUNT=", //Set the output mask for recordcount which is set in foother of the output file.
        "--FILENAMESQL=", //Get the filename by passing query from script.
        "--FILEHEADERSQL=", //TO pass query from script to set the header in the output file
        "--REPLACESTR=", //Replace String (Set of string passed by script)
        "--PRICEROWCTCHK=", //(For Prices)Check the row count in output file with record count of tables and if any discrepancy between table and output feed log that.
        "--FILEFOOTERCHK=", //Flag to check file footer in output feed (To check file is not truncated  for any reason)
        "--ZEROROWCHK=", //Flag for empty record set.        
        "--FEEDOVERWRITE=",
        "--FILEEXTENSIONHEADER"};            //Flag for overwrite if file exists.
    private String[] TagParm = new String[Tags.length];
    private String[] gDeclares = null;
    private String[] gQueries = null;
    private String[] gSetQueries = null;
    private String[] gSetOverride = null;
    private String[] gSetVar = null;
    private String[] gSetValues = null;
    private int totrec = 0;
    private boolean logflag = false;
    private String mrkCd = null;
    private String TmpFilename = null;
    private String LogFileName = AppPath + sToday + ".log";
    private boolean isfiletidy = false;
    private boolean islogfile = false;
    jFrmMsg frm = new jFrmMsg();

    public Main(String args[]) {

        // Set Look and Feel to Native style.
        try {
            javax.swing.UIManager.setLookAndFeel(javax.swing.UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            //System.out.println(e);
        }

        // Create File Chooser
        jfc = new JFileChooser();
        // Init GUI Companents
        initComponents();
        setTitle(sAppName + " - " + sVersion);
        jEditorPane1.setText("--nooutput= y \n --#");
        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        // setBounds((screenSize.width-800)/2, (screenSize.height-600)/2, 800, 600);

        // Process Command Line Arguments
        if (args.length == 0) {
            args = new String[]{"-s", "1"};
            //-m -s HAT_MY_Diesel2 -p "(select @ffdate:= ('2013/04/26') p) p1," -f o:\prodman\salesfeeds\wca\680\eod\ivan_eod.sql -d 
            args = new String[]{"-m", "-s", "HAT_MY_Diesel", "-f",
                "o:\\auto\\scripts\\mysql\\srf\\667_hst.sql",
                //            "-p", "set @ffdate=(select date_sub(max(acttime), interval '20' minute) from wca.tbl_opslog)",
                "-d", "n:\\srf\\667\\hist\\new\\", //            "-sfx", "select concat('_',(select seq from wca.tbl_opslog order by acttime desc limit 1));",
        //            "-fht", "no",
        //            "-fnm", "select '2013-05-14';",
        //            "-fft", "select concat('cnt=',count(*),'.') from wca.tbl_opslog;"
        //args = new String[] {"-m","-s","HAT_MY_Diesel2","-f", "c:\\docs\\680_VEOD.sql","-d","c:\\docs\\"};
        };
        }

        try {
            DateFormat sdf = null;
            sdf = new SimpleDateFormat("yyyyMMdd");
            sToday = sdf.format(new java.util.Date());
            //get Connection first
            for (int i = 0; args.length > i; i++) {
                // Open a database connection
                if (args[i].equalsIgnoreCase("-s")) {

                    //Getting the connection details
                    String[] ConDetail = GetConDetail.GetConnDetail(args[i + 1]);
                    driver = ConDetail[3];
                    if (driver.trim().compareToIgnoreCase("mysql") == 0) {
                        mysql = new MySqlConnection(ConDetail[0], ConDetail[1], "", ConDetail[2]);
                        conn = mysql.getConnection();
                    } else if (driver.trim().compareToIgnoreCase("mssql") == 0) {
                        mssql = new MsSqlConnection(ConDetail[0], ConDetail[1], "master", ConDetail[2]);
                        conn = mssql.getConnection();
                    }

                } else if (args[i].equalsIgnoreCase("-f")) {
                    TmpFilename = args[i + 1];
                    chkOpenOutput.setEnabled(true);
                    chkQuery.setEnabled(true);
                    chkPreview.setEnabled(true);
                } else if (args[i].equalsIgnoreCase("-v")) {
                    // Auto Run With Output validation
                    chkOpenOutput.setEnabled(true);
                    chkOpenOutput.setSelected(true);
                    bAutoRun = true;
                    bAutoManRun = true;
                } else if (args[i].equalsIgnoreCase("-a")) {
                    // Auto Run No Validation
                    chkOpenOutput.setEnabled(false);
                    chkOpenOutput.setSelected(false);
                    chkQuery.setEnabled(false);
                    chkQuery.setSelected(false);
                    bAutoRun = true;
                    bAutoManRun = true;
                } else if (args[i].equalsIgnoreCase("-q")) {
                    chkQuery.setSelected(true);
                } else if (args[i].equalsIgnoreCase("-o")) {
                    chkOpenOutput.setSelected(true);
                } else if (args[i].equalsIgnoreCase("-lx")) {
                    slineFormat = "\n";
                } else if (args[i].compareToIgnoreCase("NTD") == 0 || args[i].compareToIgnoreCase("HOL") == 0) {
                    Notify = "_" + args[i];
                } else if (args[i].equalsIgnoreCase("-m")) {
                    // Auto run wirh validation
                    chkOpenOutput.setEnabled(false);
                    chkOpenOutput.setSelected(false);
                    chkQuery.setEnabled(false);
                    chkQuery.setSelected(false);
                    bAutoRun = true;
                } else if (args[i].equalsIgnoreCase("-d")) {
                    filepath = args[i + 1].trim();
                } else if (args[i].equalsIgnoreCase("-l")) {
                    LogFileName = args[i + 1].trim();
                    islogfile = true;
                } else if (args[i].equalsIgnoreCase("-p")) {
                    String s = args[i + 1].trim();
                    gSetOverride = s.split(";");
                } else if (args[i].equalsIgnoreCase("-fnm")) {
                    ParaFileName = args[i + 1].trim();

                } else if (args[i].equalsIgnoreCase("-pfx")) {
                    ParaFileNamePrefix = args[i + 1].trim();

                } else if (args[i].equalsIgnoreCase("-sfx")) {
                    ParaSuffix = args[i + 1].trim();
                } else if (args[i].equalsIgnoreCase("-fex")) {
                    ParafileExt = args[i + 1].trim();
                } else if (args[i].equalsIgnoreCase("-fht")) {
                    ParafileHeaderText = args[i + 1].trim();
                } else if (args[i].equalsIgnoreCase("-fft")) {
                    ParafileFooterText = args[i + 1].trim();
                }

            }

            if (!islogfile) {
                LogFileName = filepath + "log.log";
            }

            if (TmpFilename != null) {
                File loadFile = new File(TmpFilename);
                if (loadFile.exists()) {
                    //remove on 01/02/2007 (Check for P04 file)
                    /*if (TmpFilename.toLowerCase().indexOf("p04_")>0) {
                 mrkCd = TmpFilename.substring(TmpFilename.toLowerCase().indexOf("p04"),TmpFilename.toLowerCase().indexOf("."));
                 mrkCd =mrkCd.replaceAll("p04_","");
                 //logflag = true;
                 }*/

                    bLoadFile = true;
                    sLoadFile = loadFile.getAbsoluteFile().toString();
                    mnuOpenScriptActionPerformed(null);

                } else if (bAutoManRun) {
                    // writelogfile(LogFileName,"SQL file not found: "+ TmpFilename);
                    writelogfile(LogFileName, "SQL file not found: " + TmpFilename);
                    System.exit(1);
                } else {
                    javax.swing.JOptionPane.showMessageDialog(null, "Unable to find the file " + loadFile.getAbsoluteFile(), "File Not Found", javax.swing.JOptionPane.WARNING_MESSAGE);
                    writelogfile(LogFileName, "SQL file not found: " + TmpFilename);
                    mnuOpenScriptActionPerformed(null);
                }
            } else if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "SQL Script has not been defined");
                exitForm(null);
            } else {
                writelogfile(LogFileName, "SQL Script has not been defined");
                System.exit(1);
            }
            if (conn == null) {
                mssql = new MsSqlConnection("sa", "", "master");
                conn = mssql.getConnection();
                driver = "mssql";
                if (conn == null) {
                    if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + TmpFilename + "Can not connect to server");
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + TmpFilename + "Can not connect to server");
                        System.exit(1);
                    }
                }
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                javax.swing.JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + TmpFilename + " Exe Parameter Failed: " + e.getMessage(), "Invalid Exe Paramenter", javax.swing.JOptionPane.WARNING_MESSAGE);
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + TmpFilename + " Exe Parameter Failed: " + e.getMessage());
                System.exit(1);
            }
        }
        if (bAutoRun) {
            btnRunNowActionPerformed(null);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        pnlRefresh = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        btnRunNow = new javax.swing.JButton();
        chkPreview = new javax.swing.JCheckBox();
        chkOpenOutput = new javax.swing.JCheckBox();
        chkQuery = new javax.swing.JCheckBox();
        cboSrvr = new javax.swing.JComboBox();
        cboIP = new javax.swing.JComboBox();
        jPanel3 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txtScriptPath = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txtOutputPath = new javax.swing.JTextField();
        jSplitPane1 = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jEditorPane1 = new javax.swing.JEditorPane();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel4 = new javax.swing.JPanel();
        prgBar = new javax.swing.JProgressBar();
        txtStatus = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        mnuOpenScript = new javax.swing.JMenuItem();
        mnuCloseScript = new javax.swing.JMenuItem();
        mnuSaveScript = new javax.swing.JMenuItem();
        mnuSaveScriptAs = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JSeparator();
        mnuExit = new javax.swing.JMenuItem();

        setTitle("SQL Feed Generator V1.1");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        pnlRefresh.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel1.setMaximumSize(new java.awt.Dimension(170, 2147483647));
        jPanel1.setMinimumSize(new java.awt.Dimension(170, 63));
        jPanel1.setPreferredSize(new java.awt.Dimension(170, 63));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnRunNow.setText("Run Now");
        btnRunNow.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRunNowActionPerformed(evt);
            }
        });
        jPanel1.add(btnRunNow, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 140, -1));

        chkPreview.setText("Show Results");
        chkPreview.setFocusable(false);
        chkPreview.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkPreviewItemStateChanged(evt);
            }
        });
        jPanel1.add(chkPreview, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 150, -1));

        chkOpenOutput.setText("Open Output File");
        jPanel1.add(chkOpenOutput, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 150, 20));

        chkQuery.setText("Query Mode");
        chkQuery.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                chkQueryItemStateChanged(evt);
            }
        });
        jPanel1.add(chkQuery, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 110, -1));

        cboSrvr.setEnabled(false);
        cboSrvr.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cboSrvrItemStateChanged(evt);
            }
        });
        jPanel1.add(cboSrvr, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 180, 110, -1));

        cboIP.setEnabled(false);
        jPanel1.add(cboIP, new org.netbeans.lib.awtextra.AbsoluteConstraints(15, 240, 140, -1));

        jPanel2.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 100, 160, 410));

        jPanel3.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jPanel3.setMinimumSize(new java.awt.Dimension(450, 60));
        jPanel3.setPreferredSize(new java.awt.Dimension(250, 60));
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Script Path:");
        jPanel3.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 70, 20));

        txtScriptPath.setEditable(false);
        jPanel3.add(txtScriptPath, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 10, 360, 20));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Output Path:");
        jPanel3.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 70, 20));

        txtOutputPath.setEditable(false);
        jPanel3.add(txtOutputPath, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 40, 360, 20));

        jPanel2.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 620, 90));

        jSplitPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        jSplitPane1.setDividerLocation(300);
        jSplitPane1.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jSplitPane1.setResizeWeight(0.1);
        jSplitPane1.setFont(new java.awt.Font("MS Sans Serif", 1, 14)); // NOI18N

        jScrollPane1.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jEditorPane1.setFont(new java.awt.Font("Courier New", 0, 12)); // NOI18N
        jScrollPane1.setViewportView(jEditorPane1);

        jSplitPane1.setLeftComponent(jScrollPane1);
        jSplitPane1.setBottomComponent(jTabbedPane1);

        jPanel2.add(jSplitPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 100, 470, 410));

        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        prgBar.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        prgBar.setMaximumSize(new java.awt.Dimension(170, 22));
        prgBar.setMinimumSize(new java.awt.Dimension(170, 22));
        prgBar.setPreferredSize(new java.awt.Dimension(170, 22));
        jPanel4.add(prgBar, new org.netbeans.lib.awtextra.AbsoluteConstraints(480, 0, 150, -1));

        txtStatus.setFont(new java.awt.Font("MS Sans Serif", 1, 14)); // NOI18N
        txtStatus.setText("StatusText");
        txtStatus.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        txtStatus.setMaximumSize(new java.awt.Dimension(1200, 22));
        txtStatus.setMinimumSize(new java.awt.Dimension(300, 22));
        txtStatus.setPreferredSize(new java.awt.Dimension(78, 22));
        jPanel4.add(txtStatus, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 430, -1));

        jPanel2.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 510, -1, -1));

        pnlRefresh.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 532));

        getContentPane().add(pnlRefresh, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 650, 560));

        jMenu1.setText("File");

        mnuOpenScript.setText("Open Script");
        mnuOpenScript.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuOpenScriptActionPerformed(evt);
            }
        });
        jMenu1.add(mnuOpenScript);

        mnuCloseScript.setText("Close Script");
        mnuCloseScript.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mnuCloseScriptActionPerformed(evt);
            }
        });
        jMenu1.add(mnuCloseScript);

        mnuSaveScript.setText("Save Script");
        mnuSaveScript.setEnabled(false);
        jMenu1.add(mnuSaveScript);

        mnuSaveScriptAs.setText("Save Script As...");
        mnuSaveScriptAs.setEnabled(false);
        jMenu1.add(mnuSaveScriptAs);
        jMenu1.add(jSeparator1);

        mnuExit.setMnemonic('X');
        mnuExit.setText("Exit");
        jMenu1.add(mnuExit);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-649)/2, (screenSize.height-603)/2, 649, 603);
    }// </editor-fold>//GEN-END:initComponents

    private void cboSrvrItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cboSrvrItemStateChanged
        cboIP.removeAllItems();
        if (cboSrvr.getSelectedIndex() == 1) {
            cboIP.addItem("SqlSvr1");
            cboIP.addItem("SqlSvr2");
            cboIP.addItem("89.187.67.59");
            cboIP.addItem("66.165.133.153");
        } else if (cboSrvr.getSelectedIndex() == 2) {
            cboIP.addItem("192.168.12.160");
            cboIP.addItem("192.168.12.109");
        }
    }//GEN-LAST:event_cboSrvrItemStateChanged

    private void chkQueryItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkQueryItemStateChanged
        if (chkQuery.isSelected()) {
            chkPreview.setSelected(true);
            chkOpenOutput.setEnabled(false);
            chkOpenOutput.setSelected(false);
            bNoOutput = true;
            bWriteOutput = false;
            cboSrvr.setEnabled(true);
            cboIP.setEnabled(true);
            cboSrvr.addItem("Select Server");
            cboSrvr.addItem("Mssql");
            cboSrvr.addItem("Mysql");
        } else {
            chkPreview.setSelected(false);
            chkOpenOutput.setEnabled(true);
            bNoOutput = false;
            bWriteOutput = true;
            cboSrvr.setEnabled(false);
            cboIP.setEnabled(false);

        }

    }//GEN-LAST:event_chkQueryItemStateChanged

    private void chkPreviewItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_chkPreviewItemStateChanged
        if (chkPreview.isSelected()) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(this, "The preview data will not be the same as the date outputed, as the \n"
                        + "preview show the real data returned from the database server and not the \n"
                        + "data after it has been rendered.");
            }
        }
    }//GEN-LAST:event_chkPreviewItemStateChanged

    private void mnuCloseScriptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuCloseScriptActionPerformed
        jTabbedPane1.removeAll();
        jTabbedPane1.paint(jTabbedPane1.getGraphics());
        jEditorPane1.setText("");
        txtScriptPath.setText("");
        txtOutputPath.setText("");
        prgBar.setValue(0);
        statusText("");
    }//GEN-LAST:event_mnuCloseScriptActionPerformed

    private void btnRunNowActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRunNowActionPerformed
        // Clear Tabpane

        String psw = "";
        int iRsRows = 0;
        jTabbedPane1.removeAll();
        jTabbedPane1.paint(jTabbedPane1.getGraphics());
        if (bAutoRun) {
            frm.setTitle(sAppName + " - " + sVersion);
            frm.setMsg("Script file: " + txtScriptPath.getText(), "Output file: " + txtOutputPath.getText());
            frm.Refresh();
            frm.setVisible(true);
            frm.Refresh();
        } else if (cboSrvr.getSelectedIndex() == 1) {
            if ((cboIP.getSelectedIndex() == 2) || (cboIP.getSelectedIndex() == 3)) {
                psw = "K376:lcnb";
            }
            mssql = new MsSqlConnection("sa", psw, "wca", cboIP.getSelectedItem().toString());
            conn = mssql.getConnection();
            driver = "mssql";
        } else if (cboSrvr.getSelectedIndex() == 2) {
            mysql = new MySqlConnection("sa", "", "prices", cboIP.getSelectedItem().toString());
            conn = mysql.getConnection();
            driver = "mysql";
        }
        disableControls();
        assignFileName();
        jEditorPane1.setEditable(false);

        //Query only No Output file
        if (chkQuery.isSelected()) {
            bNoOutput = true;
            bWriteOutput = false;
        } else {
            if (bAutoRun) {
                frm.setStatus("assigning All Settings....");
            }
            assignAllSets();
        }

        //processSetTag(); 
        //{{remove on 05/02/2007 (checking for "set @")}}
        Refresh();
        gSetQueries = splitSetQueries();
        Refresh();

        Refresh();
        gQueries = splitQueries();
        Refresh();
        // ReplaceSetTag(); 
        //{{remove on 05/02/2007 (checking for "set @")}}
        Refresh();
        fileTidy();
        //set Variables
        jEditorPane1.setCursor(new Cursor(java.awt.Cursor.WAIT_CURSOR));
        if (bAutoRun) {
            frm.setStatus("Executing Queries");
        }
        statusText("Executing Queries");
        jEditorPane1.setEditable(false);
        String strSql = jEditorPane1.getText();
//        try {
//            conn.setAutoCommit(false);
//            stmt2 = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
//            for (int i=0;i<gSetQueries.length;i++){
//                //stmt2.executeQuery();
//
//                if(gSetQueries[i].toLowerCase().indexOf("select")>=0)
//                    stmt2.executeQuery(gSetQueries[i]);                     
//                else
//                   stmt2.addBatch(gSetQueries[i]); 
//            }
//            
//            stmt2.executeBatch();
//            conn.commit();
//            
//        } catch (Exception e) {
//            statusText("Errors while executing Set query:\t" + e);
//            jEditorPane1.setEditable(true);
//            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
//            if (!bAutoManRun) {
//                JOptionPane.showMessageDialog(null, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
//                exitForm(null);
//            } else {
//                writelogfile(LogFileName, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
//                System.exit(1);
//            }
//        }

        try {
            if (gSetOverride != null) {
                conn.setAutoCommit(false);
                stmt2 = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
                for (int i = 0; i < gSetOverride.length; i++) {
                    //stmt2.executeQuery();

                    try {
                        stmt2.addBatch(gSetOverride[i]);
                        stmt2.executeBatch();
                        conn.commit();
                    } catch (Exception e) {
                        String split[] = gSetOverride[i].split("=");
                        rf = stmt2.executeQuery(split[1]);
                        rf.beforeFirst();
                        if (rf.next()) {
                            try {
                                String res = split[0] + " = '" + rf.getString(1) + "'";
                                try {
                                    stmt2.addBatch(res);
                                    stmt2.executeBatch();
                                    conn.commit();
                                } catch (Exception f) {
                                }
                            } catch (Exception res) {
                                System.out.println(e.getMessage());
                            }
                        }

                    }

//                if(gSetOverride[i].toLowerCase().indexOf("select")>=0)
//                    stmt2.executeQuery(gSetOverride[i]);                     
//                else
//                   stmt2.addBatch(gSetOverride[i]); 
                }
            }
        } catch (Exception e) {
            statusText("Errors while executing Set query:\t" + e);
            jEditorPane1.setEditable(true);
            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
                System.exit(1);
            }
        }

        // RUN NOW
        jEditorPane1.setCursor(new Cursor(java.awt.Cursor.WAIT_CURSOR));
        if (bAutoRun) {
            frm.setStatus("Executing Queries");
        }
        statusText("Executing Queries");
        jEditorPane1.setEditable(false);
        strSql = jEditorPane1.getText();
        try {
            // Create required ScrollPane for Tabpane
            JScrollPane scrPane[] = new JScrollPane[gQueries.length];
            JTable tblResult[] = new JTable[gQueries.length];
            for (int qe = 0; qe < gQueries.length; qe++) {
                if (gQueries[qe] == null) {
                    break;
                }
                if (bAutoRun) {
                    frm.setStatus("Executing Query " + (qe + 1) + ", Please wait...");
                }
                statusText("Executing Query " + (qe + 1) + ", Please wait...");
                //this.paint(this.getGraphics());
                Refresh();
                System.out.println(gQueries[qe]);

                //   gQueries[qe] = processSetTag(gQueries[qe]);
                gQueries[qe] = gQueries[qe].toLowerCase();
                //gQueries[qe] = gQueries[qe].replaceAll("from", " from "+replaceFrom+" ");
                //rf = ExecuteSql(dbs[qe],gQueries[qe]); {remove on 05/02/2007 "USE query"}
                System.out.println(gQueries[qe]);

                rf = ExecuteSql(null, gQueries[qe]);
                System.out.println(rf.getFetchSize());
                ResultSet rs = ExecuteSql(null, "SELECT count(*) as cnt;");
                if (bAutoRun) {
                    frm.setStatus("Query " + (qe + 1) + " finished, Please wait...");
                }
                statusText("Query " + (qe + 1) + " finished , Please wait...");
                Refresh();

                //rf.last();
                //Refresh();
//                iRsRows+= rf.getRow();                
//                RecCnt = Integer.toString(iRsRows);
                while (rs.next()) {
                    iRsRows = rs.getInt("cnt");
                    RecCnt = Integer.toString(iRsRows);
                }

                frm.progress.setMinimum(0);
                frm.progress.setString(null);
                frm.progress.setStringPainted(true);
                frm.progress.setValue(0);
                frm.progress.setMaximum(iRsRows);
                Refresh();

                if (logflag) {
                    totrec += rf.getRow();
                }
                //rf.beforeFirst();
                if (bAutoRun) {
                    frm.setStatus("Outputting fileHeaders....");
                }
                if (qe == 0) {
                    OutputString(fileHeader(iRsRows), false);
                }
                if (bWriteOutput) {
                    if (bAutoRun) {
                        frm.setStatus("Outputting Results to file, Please wait.......");
                    }
                }
                OutputResult(rf, true);
                if (chkPreview.isSelected()) {
                    statusText("Populating Preview tables, Please Wait...");
                    tblResult[qe] = populateTable(rf, tblResult[qe]);
                    tblResult[qe].setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    scrPane[qe] = new JScrollPane();
                    scrPane[qe].setViewportView(tblResult[qe]);
                    // Add ScrollPane to Tabpane
                    jTabbedPane1.addTab(rstab[qe].toUpperCase(), scrPane[qe]);
                } else {
                    Vector rows = new Vector();
                    Vector colnames = new Vector();
                }
                jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
                jEditorPane1.setEditable(true);
                statusText(iRsRows + " records");

                //System.out.println(iRsRows+" records return for query \n"+gQueries[qe]);
            }
            if (bWriteOutput) {
                OutputString(fileFooter(), true);
                Refresh();
                if (chkOpenOutput.isSelected()) {
                    com.edi.common.shell sh = new com.edi.common.shell();
                    String cmdLine = "c:\\program files\\TextPad 4\\TextPad.exe " + txtOutputPath.getText();
                    sh.RunIt(cmdLine);
                }
            }

            if (logflag) {
                CompareCounts();
            }

            //Checking for ENDOFFILE
            CheckEndOfFile(txtOutputPath.getText());

            if (bAutoRun) {
                frm.setStatus("Archiving the file, Please wait.......");
            }
            Archive();
            statusText("Finished processing script");
            if (chkPreview.isSelected()) {
                statusText("" + iRsRows + " Record(s) found, but only upto 1000 shown in the grid");
            } else {
                if (bAutoRun) {
                    frm.setStatus("" + iRsRows + " Record(s) processed");
                }
                statusText("" + iRsRows + " Record(s) found");
            }

            //Checking for Record Count
            zerorawchk(iRsRows);

            if (!chkOpenOutput.isSelected()) {
                if (bAutoRun) {
                    frm.setStatus("Finished processing script, now the program will exit.......");
                    exitForm(null);
                } else {
                    javax.swing.JOptionPane.showMessageDialog(this, "Finished processing script " + txtScriptPath.getText());
                }

            }
            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        } catch (Exception e) {
            statusText("Errors while executing query:\t" + e);
            jEditorPane1.setEditable(true);
            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting Script! " + TmpFilename + e.getMessage());
                System.exit(1);
            }
        }
        enableControls();

    }//GEN-LAST:event_btnRunNowActionPerformed

//Cheking for Records in the file
    private void zerorawchk(int rec) {
        int Rchk = -1;
        if (getTagParameter("--ZEROROWCHK=") != null) {
            if (getTagParameter("--ZEROROWCHK=").compareToIgnoreCase("N") == 0) {
                Rchk = 1;
            } else {
                Rchk = 0;
            }
        } else {
            Rchk = 0;
        }

        if (Rchk == 0) {
            if (rec < 1) {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "!ERROR! " + filepath + sFileNamePrefix + sFileExt + " No Record Found");
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "!ERROR! " + filepath + sFileNamePrefix + sFileExt + " No Record Found");
                    System.exit(1);
                }
            }
        }
    }

//Set flag for file overwrite
    private void feedoverwrite() {
        if (getTagParameter("--FEEDOVERWRITE=") != null) {
            if (getTagParameter("--FEEDOVERWRITE=").compareToIgnoreCase("N") == 0) {
                sFileExists = false;
            } else {
                sFileExists = true;
            }
        } else {
            sFileExists = true;
        }

    }

    private void assignFileName() {
        try {
            String tmp = "";
            //assigning filenameprefix
            tmp = getTagParameter("--FILENAMEPREFIX=");
            if (tmp == null) {
                tmp = "";
            }
            sFileNamePrefix = tmp + "*";
            tmp = "";

            //assigning file extension
            tmp = getTagParameter("--FILEEXTENSION=");
            if (tmp == null) {
                tmp = "";
            }
            sFileExt = tmp;
            tmp = "";

        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + TmpFilename + " Assigning FileName: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + TmpFilename + " Assigning FileName: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    public void buildFilename() {
        if (bNoOutput) {
            return;
        }
        String sTemp = "";
        String tmpsql = "";
        DateFormat sdf = null;
        try {
            for (int t = 0; t < Tags.length; t++) {
                //File prefix
                if (Tags[t].startsWith("--FILENAMEPREFIX=")) {
                    if (TagParm[t] != null) {
                        TagParm[t] = TagParm[t].trim();
                        FileNamePrefix = TagParm[t];
                    }
                }
                //File Name
                if (Tags[t].startsWith("--FILENAME=")) {
                    if (TagParm[t] != null) {
                        TagParm[t] = TagParm[t].trim().toUpperCase();
                        if (TagParm[t].indexOf("YYYYMMDD") != -1) {
                            sdf = new SimpleDateFormat("yyyyMMdd");
                            sTemp = sdf.format(new java.util.Date());
                            //need to have server time instead
                            filename = TagParm[t].replaceAll("YYYYMMDD", sTemp);
                            fldDate = filename;
                        } else if (TagParm[t].indexOf("YYMMDD") != -1) {
                            sdf = new SimpleDateFormat("yyMMdd");
                            sTemp = sdf.format(new java.util.Date());
                            //need to have server time instead
                            filename = TagParm[t].replaceAll("YYMMDD", sTemp);
                            fldDate = filename;
                        } else {
                            filename = TagParm[t];
                        }
                    }
                }
//                // Filename INCREMENTAL
                if (Tags[t].startsWith("--INCREMENTAL=")) {
                    if ((TagParm[t] != null) && (TagParm[t].compareToIgnoreCase("n") > 0)) {
                        TagParm[t] = TagParm[t].trim();
                        rf = ExecuteSql(null, TagParm[t]);
                        rf.beforeFirst();
                        while (rf.next()) {
                            //filename = rf.getString("FeedDate").toString();
                            seq = "_" + rf.getString("Seq").toString();
                            fldDate = filename;
                            //System.out.println(filename);
                        }
                    }

                }
                // Filename Suffix
                if (Tags[t].startsWith("--SUFFIX=")) {
                    if (TagParm[t] != null) {
                        TagParm[t] = TagParm[t].trim();
                        Suffix = TagParm[t].trim();
                    }
                }
                // FILENAMEALT
                if (Tags[t].startsWith("--FILENAMEALT=")) {
                    
                    if (Notify.endsWith("NTD") || Notify.endsWith("HOL")) {
                    } else if (TagParm[t] != null) {
                        
                        TagParm[t] = TagParm[t].trim();
                        //System.out.println( TagParm[t] );
                        
                        /*rf = ExecuteSql(topdb, TagParm[t]);
                        rf.beforeFirst();
                        while (rf.next()) {
                            filename = rf.getString(1);
                            fldDate = filename;
                        }*/
                    }
                }
                // FILENAMESQL
                if (Tags[t].startsWith("--FILENAMESQL=")) {
                    if (TagParm[t] != null) {
                        TagParm[t] = TagParm[t].trim();
                        rf = ExecuteSql(topdb, TagParm[t]);
                        rf.beforeFirst();
                        while (rf.next()) {
                            filename = rf.getString(1);
                        }
                    }
                }
                // Filename Extension

                if (Tags[t].startsWith("--FILEEXTENSION=")) {
                    if (TagParm[t] != null) {
                        fileExt = TagParm[t];
                    }
                }
                
                if (Tags[t].startsWith("--FILEEXTENSIONHEADER=")) {
                    if (TagParm[t] != null) {
                        ParafileHeaderExt = TagParm[t];
                    }
                }
                
            }
            if (ParaFileNamePrefix != null) {
                if (ParaFileNamePrefix.equalsIgnoreCase("no")) {
                    FileNamePrefix = "";
                } else if (ParaFileNamePrefix != null) {
                    ParaFileNamePrefix = ParaFileNamePrefix.trim();
                    rf = ExecuteSql(topdb, ParaFileNamePrefix);
                    rf.beforeFirst();
                    while (rf.next()) {
                        FileNamePrefix = rf.getString(1);
                    }
                }
            }
            if (ParaFileName != null) {
                if (ParaFileName.equalsIgnoreCase("no")) {
                    filename = "";
                } else if (ParaFileName != null) {
                    ParaFileName = ParaFileName.trim();
                    rf = ExecuteSql(topdb, ParaFileName);
                    rf.beforeFirst();
                    while (rf.next()) {
                        filename = rf.getString(1);
                    }
                }
            }
            if (ParaSuffix != null) {
                if (ParaSuffix.equalsIgnoreCase("no")) {
                    Suffix = "";
                } else if (ParaSuffix != null) {
                    ParaSuffix = ParaSuffix.trim();
                    rf = ExecuteSql(topdb, ParaSuffix);
                    rf.beforeFirst();
                    while (rf.next()) {
                        Suffix = rf.getString(1);
                    }
                }
            }
            if (ParafileExt != null) {
                if (ParafileExt.equalsIgnoreCase("no")) {
                    fileExt = "";
                } else if (ParafileExt != null) {
                    ParafileExt = ParafileExt.trim();
                    rf = ExecuteSql(topdb, ParafileExt);
                    rf.beforeFirst();
                    while (rf.next()) {
                        fileExt = rf.getString(1);
                    }
                }
            }

            if (CheckDir(filepath + FileNamePrefix + filename + Suffix + seq + fileExt)) {
                txtOutputPath.setText(filepath + FileNamePrefix + filename + Suffix + seq + fileExt);
                File ScrFile = new File(txtOutputPath.getText().trim());
                if (ScrFile.exists()) {
                    feedoverwrite();
                    if (sFileExists) {
                        ScrFile.delete();
                        bgotFilename = true;
                    } else {
                        if (!bAutoManRun) {
                            JOptionPane.showMessageDialog(null, "File is already exists " + txtOutputPath.getText().trim());
                            exitForm(null);
                        } else {
                            writelogfile(LogFileName, "File is already exists " + txtOutputPath.getText().trim());
                            System.exit(1);
                        }
                        bgotFilename = false;
                    }
                }
            } else {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "Could not found Output Directory " + filepath);
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "Could not found Output Directory " + filepath);
                    System.exit(1);
                }
                bgotFilename = false;
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(this, "!ERROR Exceuting! " + TmpFilename + " The output filename is invalid, so the \noutput has been supressed, Error :" + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + TmpFilename + " The output filename is invalid, so the \noutput has been supressed, Error :" + e.getMessage());
                System.exit(1);
            }
            bWriteOutput = false;
        }
    }

    private void CheckEndOfFile(String fname) {
        try {
            int pFooter = -1;
            String sChk = getTagParameter("--FILEFOOTERCHK=");

            if (sChk != null) {
                if (sChk.compareToIgnoreCase("N") == 0) {
                    pFooter = 1;
                } else {
                    pFooter = 0;
                }
            } else {
                pFooter = 0;
            }

            if (pFooter == 0) {
                int chk = 0;
                String tmpString = "";
                tmpString = getTagParameter("--FILEFOOTERTEXT=");
                if (tmpString != null) {
                    if (tmpString.length() > 0) {
                        String str = "";
                        BufferedReader in = new BufferedReader(new FileReader(fname));
                        while ((str = in.readLine()) != null) {
                            if (str.indexOf(tmpString) != -1) {
                                chk++;
                            }
                        }
                        in.close();
                        if (chk == 0) {
                            if (!bAutoManRun) {
                                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " File Truncted");
                                exitForm(null);
                            } else {
                                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " File Truncted");
                                System.exit(1);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " CheckEndOfFile: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " CheckEndOfFile: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    private void mnuOpenScriptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mnuOpenScriptActionPerformed
        jEditorPane1.setCursor(new Cursor(java.awt.Cursor.WAIT_CURSOR));
        String fileName = "";
        if (!bLoadFile) {
            jfc.setCurrentDirectory(new File("O:/APPS/SQL/"));
            jfc.showOpenDialog(this);
            fileName = jfc.getSelectedFile().toString();

        } else {
            fileName = sLoadFile;
            bLoadFile = false;
        }

        // Open a Script for either editing or execution
        try {
            //fileName = "C:\\TEST.SQL";
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            txtScriptPath.setText(fileName);
            //BufferedReader in = new BufferedReader(new FileReader("o:\\Apps\\sql\\Prices\\P10_daily.sql"));
            //O:\Apps\sql\Prices
            String str = null;
            jEditorPane1.setText(null);
            while ((str = in.readLine()) != null) {
                jEditorPane1.setText(jEditorPane1.getText() + "\n" + str);
            }
            in.close();
            // Set the start and end selection so script
            // is at top of page
            jEditorPane1.setSelectionStart(0);
            jEditorPane1.setSelectionEnd(0);
            //This Reads all Parameters
            Refresh();
            parseScript();
            if (!bgotFilename) {
                buildFilename();
            }

            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        } catch (IOException e) {
            jEditorPane1.setCursor(new Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        }
    }//GEN-LAST:event_mnuOpenScriptActionPerformed

    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        if (conn != null) {
            if (driver.trim().equalsIgnoreCase("mysql")) {
                mysql.Disconnect();
            } else if (driver.trim().equalsIgnoreCase("mssql")) {
                mssql.Disconnect();
            }
        }
        System.exit(0);

    }//GEN-LAST:event_exitForm

    public static void main(String args[]) {

        new Main(args).setVisible(true);

    }

    public void parseScript() {

        try {
            statusText("Parsing Script Vars.");
            this.paint(this.getGraphics());
            String tmpS = null;
            String script = jEditorPane1.getText();

            String[] scriptLine = script.split("\n");

            for (int l = 0; l < scriptLine.length; l++) {
                for (int t = 0; t < Tags.length; t++) {
                    tmpS = scriptLine[l].toUpperCase();
                    if (tmpS.indexOf(Tags[t]) > -1) {
                        if (Tags[t].endsWith("=") == true) {
                            tmpS = "" + scriptLine[l].substring(Tags[t].length());
                            tmpS = tmpS.trim();
                            if (tmpS.equalsIgnoreCase("")) {
                                TagParm[t] = null;
                            } else {
                                TagParm[t] = scriptLine[l].substring(Tags[t].length());
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            statusText("ParseScript:\t" + e);
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + TmpFilename + " ParseScript: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + TmpFilename + " ParseScript: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    public void statusText(java.lang.String StatusMsg) {
        txtStatus.setText(StatusMsg);
    }

    public String[] splitQueries() {
        //replaceDeclares();
        String[] queries = null;

        try {
            statusText("Spliting Queries, Please wait...");
            this.paint(this.getGraphics());
            String script = jEditorPane1.getText();
            //remove on 01/02/2007 (Check for the "drop" & "delete" query)
            /*if (chkQuery.isSelected()){
         if ((script.toLowerCase().indexOf("drop")>=0) || (script.toLowerCase().indexOf("delete")>=0)){
         if (!bAutoRun){
         javax.swing.JOptionPane.showMessageDialog(null,"Unable to process the query, becouse it contains Drop/Delete Statement","Fetal Error",javax.swing.JOptionPane.WARNING_MESSAGE);
         exitForm(null);
         }else{
         System.exit(2);
         }
         }
         }*/
            if (script.indexOf("--#") < 0) {
                script = "--#\r\n" + script;
            }
            String[] scriptLine = script.split("\n");
            int queryCount = 0;

            //        if (chkQuery.isSelected()){
            //            for (int l=0; l< scriptLine.length; l++){
            //                scriptLine[l]= scriptLine[l];
            //            }
            //            gQueries = scriptLine;
            //            return gQueries;
            //        }
            int l = 0;
            int t = 0;
            // Count query headers
            for (t = 0; t < scriptLine.length; t++) {
                if (scriptLine[t].indexOf("--#") >= 0) {
                    queryCount++;
                }
            }

            // store queries
            queries = new String[queryCount];
            rstab = new String[queryCount];
            //dbs = new String [queryCount];
            int qc = -1;

            // find first query header
            for (t = 0; t < scriptLine.length; t++) {
                if (scriptLine[t].indexOf("--#") >= 0) {
                    l = t;
                    break;
                }
            }
            for (t = l; t < (scriptLine.length); t++) {
                if (scriptLine[t].indexOf("--#") >= 0) {
                    qc++;
                    rstab[qc] = scriptLine[t].substring(3).trim();
                    queries[qc] = "";
                    //dbs[qc] ="";
                } else //remove on 05/02/2007(checking for "USE databasename" query)
                /*if (scriptLine[t].toLowerCase().indexOf("use ")>=0){
                 dbs[qc]+= scriptLine[t].toLowerCase() + ";";
                 }else{*/ if (scriptLine[t].length() > 0) {
                    queries[qc] += scriptLine[t] + "\r\n";
                } //}
            }
            // insert declare and Set statements into queries
            //	for (int q=0; q<=qc;q++){
            //	    String sDeclare="";
            //	    String t = queries[q];
            //	    for (int d=0;d<gDeclares.length;d++){
            //		sDeclare = sDeclare + gDeclares[d];
            //	    }
            //
            //	    queries[q] = sDeclare + t;
            //	}
            return queries;
        } catch (Exception e) {
            if (!bAutoManRun) {
                javax.swing.JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Unable to process the query, becouse error occurs", "Fetal Error", javax.swing.JOptionPane.WARNING_MESSAGE);
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Fetal Error: Unable to process the query, becouse error occurs");
                System.exit(1);
            }
            return queries;
        }
    }

    public String[] splitSetQueries() {
        //replaceDeclares();
        String[] queries = null;

        try {
            statusText("Spliting Queries, Please wait...");
            this.paint(this.getGraphics());
            String script = jEditorPane1.getText();
            //remove on 01/02/2007 (Check for the "drop" & "delete" query)
            /*if (chkQuery.isSelected()){
         if ((script.toLowerCase().indexOf("drop")>=0) || (script.toLowerCase().indexOf("delete")>=0)){
         if (!bAutoRun){
         javax.swing.JOptionPane.showMessageDialog(null,"Unable to process the query, becouse it contains Drop/Delete Statement","Fetal Error",javax.swing.JOptionPane.WARNING_MESSAGE);
         exitForm(null);
         }else{
         System.exit(2);
         }
         }
         }*/
            if (script.indexOf("--@") < 0) {
                script = "--#\r\n" + script;
            }
            String[] scriptLine = script.split("\n");
            int queryCount = 0;

            int l = 0;
            int t = 0;
            // Count query headers
            for (t = 0; t < scriptLine.length; t++) {
                if (scriptLine[t].indexOf("--@") >= 0) {
                    queryCount++;
                }
            }

            // store queries
            queries = new String[queryCount];
            rstab = new String[queryCount];
            //dbs = new String [queryCount];
            int qc = -1;

            // find first query header
            for (t = 0; t < scriptLine.length; t++) {
                if (scriptLine[t].indexOf("--@") >= 0) {
                    l = t;
                    break;
                }
            }
            for (t = l; t < (scriptLine.length); t++) {
                if (scriptLine[t].indexOf("--@") >= 0) {
                    qc++;
                    rstab[qc] = scriptLine[t].substring(3).trim();
                    queries[qc] = "";
                    //dbs[qc] ="";
                } else {
                    //remove on 05/02/2007(checking for "USE databasename" query)
                    /*if (scriptLine[t].toLowerCase().indexOf("use ")>=0){
                 dbs[qc]+= scriptLine[t].toLowerCase() + ";";
                 }else{*/

                    if (scriptLine[t].indexOf("--#") >= 0) {
                        break;
                    }
                    if (scriptLine[t].length() > 0) {
                        queries[qc] += scriptLine[t] + "\r\n";
                    }
                    //}

                }
            }
            return queries;
        } catch (Exception e) {
            if (!bAutoManRun) {
                javax.swing.JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Unable to process the query, becouse error occurs", "Fetal Error", javax.swing.JOptionPane.WARNING_MESSAGE);
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Fetal Error: Unable to process the query, becouse error occurs");
                System.exit(1);
            }
            return queries;
        }
    }

    public void replaceDeclares() {
        // If declare above queries the do replacement
        // from manuel input.
        statusText("Convering Declare statements.");
        this.paint(this.getGraphics());
        String script = jEditorPane1.getText();
        String[] scriptLine = script.split("\n");

        int iDeclareCount = 0;

        for (int l = 0; l < scriptLine.length; l++) {
            scriptLine[l] = scriptLine[l].toUpperCase();
            if (scriptLine[l].startsWith("SET")) {
                // Inc declare Counter
                iDeclareCount++;
            }
            if (scriptLine[l].startsWith("DECLARE")) {
                // Inc declare Counter
                iDeclareCount++;
            }
        }

    }

    private void setShowNulls() {
        if (bNoOutput) {
            return;
        }
        try {
            tmpString = getTagParameter("--SHOWNULLS=");
            if (tmpString != null) {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    bShowNulls = true;
                } else {
                    bShowNulls = false;
                }
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setShowNulls: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setShowNulls: " + e.getMessage());
                System.exit(1);
            }
            bShowNulls = false;
        }
    }

    private void setFieldSeparator() {
        if (bNoOutput) {
            return;
        }
        tmpString = getTagParameter("--FIELDSEPARATOR=");
        if (tmpString != null) {
            sFieldSeparator = tmpString;
        }
    }

    public void setFieldHeaders() {
        if (bNoOutput) {
            return;
        }
        try {
            tmpString = getTagParameter("--FIELDHEADERS=");
            if (tmpString != null) {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    sFieldHeaders = "true";
                } else if (tmpString.trim().equalsIgnoreCase("u")) {
                    sFieldHeaders = "upper";
                } else if (tmpString.trim().equalsIgnoreCase("l")) {
                    sFieldHeaders = "lower";
                } else if (tmpString.trim().equalsIgnoreCase("f")) {
                    sFieldHeaders = "first";
                } else {
                    sFieldHeaders = "false";

                }
            }

        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setFieldHeaders: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setFieldHeaders: " + e.getMessage());
                System.exit(1);
            }
            sFieldHeaders = "false";
        }
    }

    public void setNoOutput() {
        try {
            tmpString = getTagParameter("--NOOUTPUT=");
            if (tmpString != null) {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    chkPreview.setSelected(true);
                    bNoOutput = true;
                    bWriteOutput = false;
                }
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setNoOutput: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setNoOutput: " + e.getMessage());
                System.exit(1);
            }
            bNoOutput = true;
            bWriteOutput = false;
        }
    }

    public void setForceTime() {
        if (bNoOutput) {
            return;
        }
        try {
            tmpString = getTagParameter("--FORCETIME=");
            if (tmpString != null) {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    bForceTime = true;
                } else {
                    bForceTime = false;
                }
            } else {
                bForceTime = false;
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setForceTime: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setForceTime: " + e.getMessage());
                System.exit(1);
            }
            bForceTime = false;
        }
    }

    public void setDataDateFormat() {
        if (bNoOutput) {
            return;
        }
        tmpString = getTagParameter("--DATADATEFORMAT=");
        if (tmpString != null) {
            try {
                tmpString = tmpString.toLowerCase();
                if (tmpString.indexOf("hh:") > 0) {
                    DtFormat = tmpString.substring(0, tmpString.indexOf("hh:"));
                    TmFormat = tmpString.substring(tmpString.indexOf("hh:"));

                    DtFormat = DtFormat.replace('m', 'M');
                    TmFormat = TmFormat.replace('h', 'H');
                } else {

                    DtFormat = tmpString;
                    DtFormat = DtFormat.replace('m', 'M');
                }
            } catch (Exception e) {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setDataDateFormat: " + e.getMessage());
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setDataDateFormat: " + e.getMessage());
                    System.exit(1);
                }
                DtFormat = null;
            }
        }
    }

    public void setDataTimeFormat() {
        if (bNoOutput) {
            return;
        }
        for (int t = 0; t < Tags.length; t++) {
            if (Tags[t].startsWith("--DATATIMEFORMAT=")) {
                try {
                    TagParm[t] = TagParm[t].toUpperCase();
                    if (TagParm[t].length() == 0) {
                        bRemoveTime = true;
                        bForceTime = false;
                    } else {
                        TmFormat = TagParm[t].trim();
                    }
                    bRemoveTime = false;
                    break;
                } catch (Exception e) {
                    if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setDataTimeFormat: " + e.getMessage());
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setDataTimeFormat: " + e.getMessage());
                        System.exit(1);
                    }
                    bRemoveTime = true;
                    break;
                }
            }
        }

    }

    public void setArchive() {

        if (bNoOutput) {
            return;
        }
        tmpString = getTagParameter("--ARCHIVE=");
        if (tmpString != null) {
            try {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    bArchive = true;
                    tmpString = getTagParameter("--ARCHIVEPATH=");
                    if (tmpString != null) {
                        sArchivePath = tmpString.trim();
                    } else {
                        sArchivePath = "";
                        bArchive = false;
                    }
                } else {
                    bArchive = false;
                }
            } catch (Exception e) {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setArchive: " + e.getMessage());
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setArchive: " + e.getMessage());
                    System.exit(1);
                }
                bArchive = false;
            }
        }
    }

    private void setCounterField() {
        if (bNoOutput) {
            return;
        }
        if (isTAGset("--COUNTERFIELD=")) {
            try {
                tmpString = getTagParameter("--COUNTERFIELD=");
                if (tmpString != null) {
                    CntField = tmpString;
                }
            } catch (Exception e) {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setCounterField: " + e.getMessage());
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "setCounterField: " + e.getMessage());
                    System.exit(1);
                }
                CntField = null;
            }
        }
    }

    public void Archive() {

        try {
            if (bArchive) {
                File ScrFile = new File(txtOutputPath.getText().trim());
                if (ScrFile.exists()) {
                    if (CheckDir(sArchivePath + ScrFile.getName())) {
                        File inputFile = new File(txtOutputPath.getText().trim());
                        File outputFile = new File(sArchivePath + ScrFile.getName());
                        FileReader in = new FileReader(inputFile);
                        FileWriter out = new FileWriter(outputFile);
                        int c;
                        while ((c = in.read()) != -1) {
                            out.write(c);
                        }
                        in.close();
                        out.close();
                    } else if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Archive Directory Not Found");
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Archive Directory Not Found");
                        System.exit(1);
                    }
                }
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Failed to archive: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Failed to archive: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    public void OutputResult(ResultSet rs, boolean appendfile) {
        String t = "", tmpfldval = null;
        String fieldValue = "";
        int counter = 1;
        boolean checkCnt = false;

        if (bNoOutput) {
            return;
        }

        try {
            BufferedWriter out = new BufferedWriter(new FileWriter(txtOutputPath.getText(), appendfile));
            // Do field Headers If required
            if (bFixedWidth) {
                if ((sFixedWidthOffsets.length) != (rs.getMetaData().getColumnCount())) {
                    bWriteOutput = false;
                    if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(this, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Field Offset Mismatch Fld=" + rs.getMetaData().getColumnCount() + " Off= " + sFixedWidthOffsets.length);
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " Field Offset Mismatch Fld=" + rs.getMetaData().getColumnCount() + " Off= " + sFixedWidthOffsets.length);
                        System.exit(1);
                    }
                }
            }
            if (!sFieldHeaders.equalsIgnoreCase("false")) {
                for (int field = 1; field <= rs.getMetaData().getColumnCount(); field++) {
                    if (bFixedWidth) {
                        t += processFixedWidth(rs.getMetaData().getColumnLabel(field), field);
                    } else {
                        t += rs.getMetaData().getColumnLabel(field) + sFieldSeparator;
                    }
                }
                Refresh();
                if (!bFixedWidth) {
                    t = t.substring(0, t.length() - sFieldSeparator.length());
                }

                if (sFieldHeaders.equalsIgnoreCase("true")) {
                    out.write(t + slineFormat); //"\n\r"
                } else if (sFieldHeaders.equalsIgnoreCase("upper")) {
                    out.write(t.toUpperCase() + slineFormat); //"\n\r"
                } else if (sFieldHeaders.equalsIgnoreCase("lower")) {
                    out.write(t.toLowerCase() + slineFormat); //"\n\r"
                } else if (sFieldHeaders.equalsIgnoreCase("first")) {
                    out.write(t + slineFormat); //"\n\r"
                    sFieldHeaders = "false";
                }
                //t = rs.getMetaData().getColumnLabel(rs.getMetaData().getColumnCount());
                //t = t.toUpperCase();
                //out.write(t+"\n\r");
            }

            int cnt = 0;
            // write data to file
            while (rs.next()) {
                if ((cnt % 50) == 0) {
                    this.frm.progress.setValue(cnt);
                    this.frm.processMessage.setText("Processing Record " + this.frm.progress.getValue() + " of " + this.frm.progress.getMaximum() + ".");
                    Refresh();
                }

                for (int field = 1; field <= rs.getMetaData().getColumnCount(); field++) {

                    String tmpfield = rs.getString(field);

                    //Check for counter field
                    if (CntField != null) {
                        if (CntField.compareToIgnoreCase(rs.getMetaData().getColumnLabel(field)) == 0) {
                            tmpfield = Integer.toString(counter);
                            counter++;
                        }
                    }

                    fieldValue = processFieldValue(tmpfield, rs.getMetaData().getColumnType(field));

                    if (bFixedWidth) {
                        fieldValue = processFixedWidth(fieldValue, field);
                    } else // ShowNulls
                    if (bShowNulls) {
                        fieldValue += sFieldSeparator;
                    } else if (!rs.wasNull()) {
                        fieldValue = fieldValue.trim() + sFieldSeparator;
                    } else {
                        fieldValue = "" + sFieldSeparator;
                    }

                    // Modify fieldValue ONLY if last fields from record
                    try {
                        if (field == rs.getMetaData().getColumnCount()) {
                            // Remove lastSeperator from fieldValue
                            if (!bFixedWidth) {
                                fieldValue = fieldValue.substring(0, fieldValue.length() - sFieldSeparator.length());
                            }
                            // Add Carrage Return and LineFeed to fieldValue
                            fieldValue += slineFormat;
                        }
                    } catch (NullPointerException e) {
                        fieldValue += slineFormat;
                    }
                    // Output the fieldValue to the output file
                    out.write(fieldValue);
                }
                cnt++;
            }
            out.close();

        } catch (IOException e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                System.exit(1);
            }
        } catch (SQLException e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                System.exit(1);
            }
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputResult: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    public String processFieldValue(String fieldValue, int fieldType) {
        try {
            switch (fieldType) {

                //case 1:				System.out.println("DataType: CHAR()\t:"+fieldType);		break;
                //case java.sql.Types.ARRAY:	System.out.println("DataType: ARRAY\t:"+fieldType);		break;
                //case java.sql.Types.BINARY:	System.out.println("DataType: BINARY\t:"+fieldType);		break;
                //case java.sql.Types.BIT:		System.out.println("DataType: BIT\t:"+fieldType);		break;
                //case java.sql.Types.BLOB:		System.out.println("DataType: BLOB\t:"+fieldType);		break;
                //case java.sql.Types.BOOLEAN:	System.out.println("DataType: BOOLEAN\t:"+fieldType);		break;
                //case java.sql.Types.DECIMAL:	System.out.println("DataType: MONEY\t:"+fieldType);		break;
                //case java.sql.Types.DISTINCT:	System.out.println("DataType: DISTINCT\t:"+fieldType);		break;
                //case java.sql.Types.DOUBLE:	System.out.println("DataType: DOUBLE\t:"+fieldType);		break;
                //case java.sql.Types.FLOAT:	System.out.println("DataType: FLOT\t:"+fieldType);		break;
                //case java.sql.Types.JAVA_OBJECT:	System.out.println("DataType: JAVA_OBJECT\t:"+fieldType);	break;
                //case java.sql.Types.LONGVARBINARY:System.out.println("DataType: LONGVARBINARY\t:"+fieldType);	break;
                //case java.sql.Types.LONGVARCHAR:	System.out.println("DataType: LONGVARCHAR\t:"+fieldType);	break;
                //case java.sql.Types.NULL:		System.out.println("DataType: NULL\t:"+fieldType);		break;
                //case java.sql.Types.NUMERIC:	System.out.println("DataType: NUMERIC\t:"+fieldType);		break;
                //case java.sql.Types.OTHER:	System.out.println("DataType: OTHER\t:"+fieldType);		break;
                //case java.sql.Types.REAL:		System.out.println("DataType: REAL\t:"+fieldType);		break;
                //case java.sql.Types.REF:		System.out.println("DataType: REF\t:"+fieldType);		break;
                //case java.sql.Types.STRUCT:	System.out.println("DataType: STRUCT\t:"+fieldType);		break;
                case java.sql.Types.TIMESTAMP:
                    fieldValue = processDateTime(fieldValue);
                    break;
                case java.sql.Types.TINYINT:
                    fieldValue = processINTEGERfield(fieldValue);
                    break;
                //case java.sql.Types.VARBINARY:	System.out.println("DataType: VARBINARY\t:"+fieldType);		break;
                case java.sql.Types.CHAR:
                case java.sql.Types.VARCHAR:
                case java.sql.Types.LONGVARCHAR:
                    if (fieldValue != null) {
                        if (!bFixedWidth) {
                            fieldValue = fieldValue.trim();
                        } else {
                            fieldValue = Rtrim(fieldValue);
                        }
                        fieldValue = fieldValue.replaceAll("<br/>", "<br />");
                        fieldValue = fieldValue.replaceAll("\r\n", "<br />");
                        fieldValue = fieldValue.replaceAll("\n", "<br />");
                        fieldValue = fieldValue.replaceAll("\r", "<br />");
                        fieldValue = fieldValue.replaceAll("\t", " ");

                        fieldValue = removeComment(fieldValue);
                        fieldValue = removeStrike(fieldValue);
                        fieldValue = EnablePunctuation(fieldValue);
                        fieldValue = EnableOrdinal(fieldValue);
                        fieldValue = removeHTML(fieldValue);
                        //fieldValue = AllowHighCodePage(fieldValue);
                        fieldValue = ReplaceChar(fieldValue);
                        fieldValue = addCRLF(fieldValue);
                        fieldValue = replaceStr(fieldValue);
                    }

                    break;
                case java.sql.Types.DATE:
                    fieldValue = processDateTime(fieldValue);
                    break;
                case java.sql.Types.TIME:
                    fieldValue = processDateTime(fieldValue);
                    break;
                case java.sql.Types.BIGINT:
                    fieldValue = processINTEGERfield(fieldValue);
                    break;
                case java.sql.Types.INTEGER:
                    fieldValue = processINTEGERfield(fieldValue);
                    break;
                case java.sql.Types.SMALLINT:
                    fieldValue = processINTEGERfield(fieldValue);
                    break;
                default:
                    fieldValue = fieldValue;
                    break;
                //System.out.println("DataType: UNKNOWN\t:"+fieldType);		break;
            }
        } catch (Exception e) {
            //System.out.println("processFieldValue: "+ e);
            return fieldValue;
        }

        return fieldValue;

    }

    private String Rtrim(String str) {
        if (str == null) {
            return "";
        }
        int len = str.length();
        while (str.charAt(len - 1) == -1) {
            str = str.substring(0, len - 1);
            len--;
        }
        return str;
    }

    public String ReplaceChar(String fieldValue) {

        String[] replacestr = null;
        String[] lines = null;
        int Ochar = -1, Rchar = -1;
        String str = "", str1 = "";
        if (isTAGset("--REPLACECHAR=")) {
            try {
                String sReplace = getTagParameter("--REPLACECHAR=");
                sReplace = sReplace.substring(1, sReplace.length() - 1);
                lines = sReplace.split(";");
                for (int f = 0; f < lines.length; f++) {
                    replacestr = lines[f].split(",");

                    Ochar = Integer.parseInt(replacestr[0]);
                    Rchar = Integer.parseInt(replacestr[1]);

                    str = new Character((char) Ochar).toString();
                    str1 = new Character((char) Rchar).toString();

                    fieldValue = fieldValue.replaceAll(str, str1);
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                // System.out.println("ReplaceChar: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String AllowHighCodePage(String fieldValue) {

        String str;

        if (isTAGset("--ALLOWHIGHCODEPAGE=")) {
            try {
                String sReplace = getTagParameter("--ALLOWHIGHCODEPAGE=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {
                    } else {
                        for (int f = 0; f <= 31; f++) {
                            str = new Character((char) f).toString();
                            fieldValue = fieldValue.replace(str, ""); //replace all high code page char to nothing
                        }
                        for (int f = 127; f <= 255; f++) {
                            str = new Character((char) f).toString();
                            fieldValue = fieldValue.replace(str, ""); //replace all high code page char to nothing
                        }
                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("HighCodePage: " + e);
                return fieldValue;
            }
        } else {
            try {
                for (int f = 0; f <= 31; f++) {
                    str = new Character((char) f).toString();
                    //System.out.println(str);
                    fieldValue = fieldValue.replace(str, ""); //replace all high code page char to nothing
                }
                for (int f = 127; f <= 255; f++) {
                    str = new Character((char) f).toString();
                    // System.out.println(str);
                    fieldValue = fieldValue.replace(str, ""); //replace all high code page char to nothing
                }
            } catch (Exception e) {
                //System.out.println("HighCodePage: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String EnableOrdinal(String fieldValue) {

        String str = "";
        if (isTAGset("--ENABLEORDINAL=")) {
            try {
                String sReplace = getTagParameter("--ENABLEORDINAL=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {

                        for (int f = 0; f <= 31; f++) {
                            fieldValue = fieldValue.replace("&#" + f + ";", ""); //replace all ordinal value to nothing
                        }
                        for (int f = 32; f <= 126; f++) {
                            str = new Character((char) f).toString();
                            //System.out.println(str);
                            fieldValue = fieldValue.replace("&#" + f + ";", str); //replace all ordinal value to printable character
                        }
                        for (int f = 127; f <= 255; f++) {
                            fieldValue = fieldValue.replace("&#" + f + ";", ""); //replace all ordinal value to nothing
                        }

                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("Ordinal: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String EnablePunctuation(String fieldValue) {

        if (isTAGset("--ENABLEPUNCTUATION=")) {
            try {
                String sReplace = getTagParameter("--ENABLEPUNCTUATION=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {
                        fieldValue = fieldValue.replaceAll("&nbsp;", " "); //replace non breaking space
                        fieldValue = fieldValue.replaceAll("&lt;", "<"); //replace <(less than)
                        fieldValue = fieldValue.replaceAll("&gt;", ">"); //replace >(greater than)
                        fieldValue = fieldValue.replaceAll("&quot;", "\""); //replace "(double quote)
                        fieldValue = fieldValue.replaceAll("&apos;", "'"); //replace '(single speech mark)
                        fieldValue = fieldValue.replaceAll("&eq;", "="); //replace =(equal)
                        fieldValue = fieldValue.replaceAll("&amp;", "&"); //replace &
                        fieldValue = fieldValue.replaceAll("&eacute;", "e"); //replace french e to english e
                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("Punctuation: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String addCRLF(String fieldValue) {

        if (isTAGset("--HARDLINEBREAKS=")) {
            try {
                String sReplace = getTagParameter("--HARDLINEBREAKS=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {
                        fieldValue = fieldValue.replace("<br />", "\r\n");
                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("addCRLF: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String removeHTML(String fieldValue) {

        if (isTAGset("--REMOVEHTML=")) {
            try {
                String sReplace = getTagParameter("--REMOVEHTML=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {
                        fieldValue = fieldValue.replaceAll("<[^<br>][^>]*>", "");
                        fieldValue = fieldValue.replaceAll("<[^<br>][^>]*", "");
                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("removeHTML: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String removeComment(String fieldValue) {

        if (isTAGset("--STARTCOMMENT=")) {
            try {
                String sComment = getTagParameter("--STARTCOMMENT=");
                String eComment = getTagParameter("--ENDCOMMENT=");
                if (sComment != null && eComment != null) {
                    fieldValue = fieldValue.replaceAll(sComment + ".*?" + eComment, "");
                    fieldValue = fieldValue.replaceAll(sComment + ".*", "");
                } else if (sComment != null && eComment == null) {
                    fieldValue = fieldValue.replaceAll(sComment + ".*?<br />", "");
                    fieldValue = fieldValue.replaceAll(sComment + ".*", "");
                }
                // System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("removeComment: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String removeStrike(String fieldValue) {

        if (isTAGset("--REMOVESTRIKE=")) {
            try {
                String sReplace = getTagParameter("--REMOVESTRIKE=");
                if (sReplace != null) {
                    if (sReplace.compareToIgnoreCase("y") == 0) {
                        fieldValue = fieldValue.replaceAll("<strike>.*?</strike>", "");
                    }
                }
                //System.out.println(fieldValue);
            } catch (Exception e) {
                //System.out.println("removeStrike: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String replaceStr(String fieldValue) {
        String[] replacestr = null;
        String[] lines = null;
        if (isTAGset("--REPLACESTR=")) {
            try {
                String sReplace = getTagParameter("--REPLACESTR=");
                sReplace = sReplace.substring(1, sReplace.length() - 1);
                lines = sReplace.split(";");
                for (int f = 0; f < lines.length; f++) {
                    replacestr = lines[f].split(",");
                    fieldValue = fieldValue.replaceAll(replacestr[0], replacestr[1]);
                }
                // System.out.println(fieldValue);
            } catch (Exception e) {
                // System.out.println("REPLACESTR: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String processDateTime(String fieldValue) {

        try {

            if (DtFormat != null && fieldValue != null) {
                String sReplace = DtFormat;
                sReplace += "" + TmFormat;
                DateFormat sdf = new SimpleDateFormat(sReplace);
                // Remove milsecs from datetime string
                if (fieldValue.indexOf(".") > 1) {
                    fieldValue = fieldValue.substring(0, fieldValue.indexOf("."));
                }
                fieldValue = fieldValue.replaceAll("-", "/");

                java.util.Date dDate = new java.util.Date(fieldValue);
                fieldValue = sdf.format(dDate);

                if (fieldValue.indexOf("00:") > 0) {
                    if (bForceTime == false) {
                        fieldValue = fieldValue.replaceAll("00:00:00", "");
                    }
                }
                //System.out.println(fieldValue);
            }

        } catch (Exception e) {
            //System.out.println("processDateTime: "+ e);
            return fieldValue;
        }
        return fieldValue;
    }

    public String processDATETIMEfield(String fieldValue) {
        if (isTAGset("--DATADATEFORMAT=")) {
            try {
                String sReplace = getTagParameter("--DATADATEFORMAT=");
                sReplace = sReplace.replace('Y', 'y');
                sReplace = sReplace.replace('D', 'd');
                sReplace = sReplace.replace('m', 'M');
                sReplace = sReplace + " HH:mm:ss";

                DateFormat sdf = new SimpleDateFormat(sReplace);
                // Remove milsecs from datetime string
                fieldValue = fieldValue.substring(0, fieldValue.indexOf("."));
                fieldValue = fieldValue.replaceAll("-", "/");

                java.util.Date dDate = new java.util.Date(fieldValue);
                if (bRemoveTime) {
                    fieldValue = sdf.format(dDate);
                    fieldValue = fieldValue.substring(0, fieldValue.indexOf(" "));
                } else {
                    fieldValue = sdf.format(dDate);
                }
                if (bForceTime == false) {
                    fieldValue = fieldValue.replaceAll("00:00:00", "");
                }
                //System.out.println(fieldValue);

            } catch (Exception e) {
                //System.out.println("processDATETIMEfield: " + e);
                return fieldValue;
            }
        }
        return fieldValue;
    }

    public String processDATEfield(String fieldValue) {
        return fieldValue;
    }

    public String processTIMEfield(String fieldValue) {
        return fieldValue;
    }

    public String processINTEGERfield(String fieldValue) {
        return fieldValue;
    }

    public String processFixedWidth(String fldValue, int pos) {
        String fxwValues = "";
        String pdChs = null, newVal = null, padCh = null;
        int pad = 0, t = 0, lenVal = 0, nofrep = 0;

        try {
            if (fldValue != null) {
                fldValue = fldValue;
            } else {
                fldValue = "";
            }
            Refresh();
            pad = sFixedWidthOffsets[pos - 1].toLowerCase().indexOf("z");
            pdChs = "";
            nofrep = Integer.parseInt(sFixedWidthOffsets[pos - 1].substring(2));
            Refresh();
            if (pad >= 1) {
                //Zeros used
                padCh = "0";
            } else {
                //Spaces used
                padCh = " ";
            }
            Refresh();
            for (int nof = 1; nof <= nofrep; nof++) {
                pdChs += padCh;
            }
            Refresh();
            String tmp = sFixedWidthOffsets[pos - 1].toUpperCase().substring(0, 1);
            char choice = tmp.charAt(0);
            switch (choice) {
                case 'L':
                    lenVal = fldValue.length();
                    if (lenVal > nofrep) {
                        fldValue = fldValue.substring(0, nofrep);
                        lenVal = fldValue.length();
                    }
                    newVal = fldValue + pdChs.substring(lenVal, nofrep);
                    break;
                case 'R':
                    lenVal = fldValue.length();
                    if (lenVal > nofrep) {
                        fldValue = fldValue.substring(0, nofrep);
                        lenVal = fldValue.length();
                    }
                    newVal = pdChs.substring(lenVal, nofrep) + fldValue;
                    break;
            }
            fxwValues = newVal;

        } catch (Exception e) {
            //System.out.println("processFixedWidth: "+ e);
        }
        return fxwValues;
    }

    private ResultSet ExecuteSql(String dbs, String Values) {
        ResultSet rs_s = null;
        try {
            stmt2 = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            Refresh();
            if (dbs != null) {
                stmt2.executeUpdate(dbs.toLowerCase());
            }

            //remove on 01/02/2007 (converting script in to lowercase and checking for ".dbo" in the script)
            /* if ((cboSrvr.getSelectedIndex()==2) || (cmdSrv.trim().equalsIgnoreCase("0"))){
         //Values = Values.toLowerCase();
         //Values = Values.replaceAll(".dbo", "");
         }
         Refresh();     */
            rs_s = stmt2.executeQuery(Values/*.toLowerCase()*/);
            Refresh();
        } catch (SQLException e) {
            // System.out.println("ExecuteSql: "+Values);
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " The ExecuteSql: " + e.getMessage(), "Error", 2);
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " The ExecuteSql: " + e.getMessage());
                System.exit(1);
            }
            rs_s = null;
        }
        return rs_s;
    }

    public String processSetTag(String sql) {

        String[] scriptLine = sql.split("\n");
        String[] SetVar = null;
        String[] SetValues = null;
        String query = "";
        int setcount = 0, i = 0;
        try {
            for (int l = 0; l < scriptLine.length; l++) {
                i = scriptLine[l].toLowerCase().indexOf("set @");
                if (i >= 0) {
                    setcount++;
                }
            }

            // define array elements fron setcount
            SetVar = new String[setcount];
            SetValues = new String[setcount];
            i = 0;
            for (int f = 0; f < scriptLine.length; f++) {
                if (scriptLine[f].toLowerCase().indexOf("set @") >= 0) {
                    int st = scriptLine[f].indexOf("@");
                    int end = scriptLine[f].indexOf("=", st);
                    SetVar[i] = scriptLine[f].substring(st, end).trim();
                    String str = "";
                    str = scriptLine[f].substring((scriptLine[f].indexOf("(") + 1), (scriptLine[f].indexOf(";") - 1));
                    str = str.trim();
                    if (str.endsWith(")")) {
                        str = str.substring(0, str.length() - 1);
                    }

                    System.out.println(str);
                    ResultSet rs_var = ExecuteSql(topdb, str);
                    rs_var.beforeFirst();
                    while (rs_var.next()) {
                        //SetValues[i] = rs_var.getString("mktclosedate").toString();
                        System.out.println(gSetValues[i]);
                    }
                    rs_var.close();

                    SetValues[i] = "'" + SetValues[i] + "'";
                    i++;
                } else {
                    query += scriptLine[f] + "\n";
                }
            }

            //Replace Variable with significant values
            for (int f = 0; f < SetVar.length; f++) {
                if (SetValues[f] == null) {
                    if (!bAutoRun) {
                        JOptionPane.showMessageDialog(null, "The set @ parameter returned error, possibly the database did not include in the select statement i.e From Database.dbo.table ", "Error", 2);
                    } else {
                        System.exit(6);
                        //exitwithreturnForm(6);
                    }
                } else {
                    query = query.replace(SetVar[f], SetValues[f]);
                }
            }

            //System.out.println("------->"+query);
            return query;

        } catch (Exception e) {
            System.out.println("processSetTag" + e);
            if (!bAutoRun) {
                JOptionPane.showMessageDialog(null, "processSetTag" + e);
            } else {
                System.exit(6);
                //   exitwithreturnForm(6);
            }
            return "";
        }
    }

    public void ReplaceSetTag() {
        try {
            for (int l = 0; l < gQueries.length; l++) {
                for (int i = 0; i < gSetVar.length; i++) {
                    gQueries[l] = gQueries[l].toLowerCase().replaceAll(gSetVar[i].toLowerCase(), gSetValues[i]);
                }
            }
        } catch (Exception e) {
            //System.out.println("ReplaceSetTag"+e);
        }
    }

    public void OutputString(String str, boolean appendfile) {
        if (bNoOutput) {
            return;
        }
        if (bWriteOutput) {
            if (str != null) {
                try {
                    BufferedWriter out = new BufferedWriter(new FileWriter(txtOutputPath.getText(), appendfile));
                    out.write(str);
                    out.close();
                } catch (IOException e) {
                    if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputString: " + e.getMessage());
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " OutputString: " + e.getMessage());
                        System.exit(1);
                    }
                }
            }
        }
    }

    public String fileHeader(int Rc) {

        try {
            
            String retStr = "";
            String zeros = "0000000";
            String rcn = "" + Rc;
            String fht = null;
            String sht = "";
            if (bNoOutput) {
                return retStr = null;
            }
            
            fht = getTagParameter("--FILEHEADERTEXT=");
//        if (fht != null) {
//                //Check for NNNNNNN
//                if(fht.toUpperCase().contains("NNNNNNN")){
//                    fht = fht.toUpperCase().replaceAll("NNNNNNN", zeros);
//                    fht = fht.toUpperCase().replaceAll(zeros,zeros.substring(rcn.length(),7)+rcn);
//                }else if(fht.toUpperCase().contains("NNNNNN")){
//                   /* String str = "";
//                    int Ncnt = 6-rcn.length();
//                    for(int f=0; f<Ncnt; f++){
//                        str += "0";
//                    }
//                    str += rcn;
//                    fht = Sreplace(fht, "NNNNNN", str);*/
//                    fht = fht.toUpperCase().replaceAll("NNNNNN", zeros);
//                    fht = fht.toUpperCase().replaceAll(zeros,zeros.substring(rcn.length(),6)+rcn);
//                }else if(fht.toUpperCase().contains("NNNNN")){
//                    fht = fht.toUpperCase().replaceAll("NNNNN", zeros);
//                    fht = fht.toUpperCase().replaceAll(zeros,zeros.substring(rcn.length(),5)+rcn);
//                }
            //Check for date in fileheadertext
//                if (fht.toUpperCase().contains("YYYYMMDD")){
//                    DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        fht = fht.toUpperCase().replaceAll("YYYYMMDD", fldDate);
//                    }else{
//                        fht = fht.toUpperCase().replaceAll("YYYYMMDD", sTemp);
//                    }
//                }else if (fht.toUpperCase().contains("YYMMDD")){
//                    DateFormat sdf = new SimpleDateFormat("yyMMdd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("YYMMDD", sTemp);
//                }else if (fht.toUpperCase().contains("DD/MM/YYYY")){
//                    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("DD/MM/YYYY", sTemp);
//                }else if (fht.toUpperCase().contains("DD-MM-YYYY")){
//                    DateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("DD-MM-YYYY", sTemp);
//                }else if (fht.toUpperCase().contains("YYYY/MM/DD")){
//                    DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("YYYY/MM/DD", sTemp);
//                }else if (fht.toUpperCase().contains("YYYY-MM-DD")){
//                    DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        /*java.util.Date myDate = null;
//                        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyyMMdd");
//                        myDate = sdf1.parse(fldDate);*/
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("YYYY-MM-DD", sTemp);
//                }else if (fht.toUpperCase().contains("DD/MM/YY")){
//                    DateFormat sdf = new SimpleDateFormat("dd/MM/yy");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("DD/MM/YY", sTemp);
//                }else if (fht.toUpperCase().contains("DD-MM-YY")){
//                    DateFormat sdf = new SimpleDateFormat("dd-MM-yy");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        try{
//                            sTemp = sdf.format(fldDate);
//                        }catch(Exception e){
//                            sTemp = fldDate;
//                        }
//                    }
//                    fht = fht.toUpperCase().replaceAll("DD-MM-YY", sTemp);
//                }
//        }
//        sht = getfileheader();

//            tmpString = getTagParameter("--FILEHEADERDATE=");
//            if((fht==null) && (tmpString==null) && (sht.compareToIgnoreCase("")==0)){return retStr=null;}
//            if(fht==null){fht = "";}
//            if (tmpString!=null){
//                if (tmpString.trim().equalsIgnoreCase("yyyymmdd")){
//                    DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        tmpString = tmpString.toLowerCase().replaceAll("yyyymmdd", fldDate);
//                    }else{
//                        tmpString = tmpString.toLowerCase().replaceAll("yyyymmdd", sTemp);
//                    }
//                    retStr = tmpString;
//                }else if ( tmpString.trim().equalsIgnoreCase("yymmdd")){
//                    DateFormat sdf = new SimpleDateFormat("yyMMdd");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        tmpString = tmpString.toLowerCase().replaceAll("yymmdd", fldDate);
//                    }else{
//                        tmpString = tmpString.toLowerCase().replaceAll("yymmdd", sTemp);
//                    }
//                    retStr = tmpString;
//                }else if ( tmpString.trim().equalsIgnoreCase("dd/mm/yyyy")){
//                    DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
//                    String sTemp = sdf.format(new java.util.Date());
//                    if (fldDate!=null){
//                        java.util.Date myDate = null;
//                        String frm = getTagParameter("--FILENAME=").toLowerCase();
//                        frm = frm.replace("m","M");
//                        SimpleDateFormat sdf1 = new SimpleDateFormat(frm);
//                        myDate = sdf1.parse(fldDate);
//                        tmpString = sdf.format(myDate);
//                        //tmpString = tmpString.toLowerCase().replaceAll("dd/mm/yyyy", fldDate);
//                    }else{
//                        tmpString = tmpString.toLowerCase().replaceAll("dd/mm/yyyy", sTemp);
//                    }
//                    retStr = tmpString;
//                }
//
//            }
//            
//            
//            retStr= fht +  sht +  retStr + seq + Notify;
            if (ParafileHeaderText != null) {
                if (ParafileHeaderText.equalsIgnoreCase("no")) {
                    fht = null;
                } else {
                    fht = ParafileHeaderText;
                }
            }
            if (fht != null) {
                String fileextension = fileExt.toUpperCase();

                fileextension += "_";
                fileextension = fileextension.replaceAll(".TXT_", "");
                fileextension = fileextension.replaceAll("\\.", "");
                
                /*if( ParafileHeaderExt != null ){
                
                    retStr = fht + fileextension + filename + Suffix + seq;
                }else{
                    retStr = fht + filename + Suffix + seq;
                }*/
                
                //retStr = fht + filename + Suffix + seq;
                //retStr = fht + fileextension + filename + Suffix + seq;
                
                fileextension = "";
                //retStr = fht + fileextension + filename + Suffix + seq;
                retStr = fht + fileextension + filename + seq;
                
                if (retStr.compareToIgnoreCase("") > 0) {
                    retStr += slineFormat;
                }
            } else {
                retStr = null;
            }


            /*if(sht.length()<=0){
         retStr = retStr.toUpperCase().replaceAll("NNNNNN", zeros);
         retStr = retStr.toUpperCase().replaceAll(zeros,zeros.substring(rcn.length(),6)+rcn);
         }*/
            return retStr;
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileHeader: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileHeader: " + e.getMessage());
                System.exit(1);
            }
            return "";
        }
    }

    public String fileFooter() {

        String retStr = "";
        tmpString = getTagParameter("--FILEFOOTERTEXT=");
        if (tmpString != null) {
            try {
                if (tmpString.indexOf("YYYYMMDD") >= 0) {
                    DateFormat sdf = new SimpleDateFormat("yyyyMMdd");
                    String sTemp = sdf.format(new java.util.Date());
                    tmpString = tmpString.replaceAll("YYYYMMDD", sTemp);
                    retStr = tmpString;
                } else if (tmpString.indexOf("YYMMDD") >= 0) {
                    DateFormat sdf = new SimpleDateFormat("yyMMdd");
                    String sTemp = sdf.format(new java.util.Date());
                    tmpString = tmpString.replaceAll("YYMMDD", sTemp);
                    retStr = tmpString;
                } else {
                    retStr = tmpString;
                }
                retStr += getRecCount();
            } catch (Exception e) {
                if (!bAutoManRun) {
                    JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileFooter: " + e.getMessage());
                    exitForm(null);
                } else {
                    writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileFooter: " + e.getMessage());
                    System.exit(1);
                }
                retStr = "";
            }
        }
        if (ParafileFooterText != null) {
            if (ParafileFooterText.equalsIgnoreCase("no")) {
                retStr = null;
            } else if (ParafileFooterText != null) {
                ParafileFooterText = ParafileFooterText.trim();
                try {
                    rf = ExecuteSql(topdb, ParafileFooterText);
                    rf.beforeFirst();
                    while (rf.next()) {
                        retStr = rf.getString(1);
                    }
                } catch (SQLException e) {
                    if (!bAutoManRun) {
                        JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileFooter: " + e.getMessage());
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileFooter: " + e.getMessage());
                        exitForm(null);
                    } else {
                        writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileFooter: " + e.getMessage());
                        System.exit(1);
                    }
                    retStr = null;
                }
            }
        }
        if (retStr != null) {
            if (retStr.compareToIgnoreCase("") > 0) {
                retStr += slineFormat;
            } else {
                retStr = null;
            }
        } else {
            retStr = null;
        }
        return retStr;
    }

    public String getfileheader() {
        ResultSet rsH = null;
        String Hstr = null;
        String Hretstr = "";
        Hstr = getTagParameter("--FILEHEADERSQL=");
        try {
            if (Hstr != null) {
                rsH = ExecuteSql(topdb, Hstr);
                if (rsH != null) {
                    rsH.beforeFirst();
                    while (rsH.next()) {
                        Hretstr = rsH.getString(1);
                    }
                } else {
                    Hretstr = "";
                }
            } else {
                Hretstr = "";
            }
        } catch (Exception e) {
            Hretstr = "";
            //System.out.println("FileHeaderSQL--" + e.getMessage());
        }
        return Hretstr;
    }

    public String getRecCount() {
        String retStrCnt = "";
        int diff = 0;
        tmpString = getTagParameter("--RECCOUNT=");
        if (tmpString != null) {
            try {
                diff = tmpString.length() - RecCnt.length();
                for (int f = 0; f < diff; f++) {
                    retStrCnt += "0";
                }

                retStrCnt += RecCnt;
            } catch (Exception e) {
                //System.out.println("fileFooter Tag: NULL");
                retStrCnt = "";
            }
        }
        return retStrCnt;
    }

    public boolean isTAGset(java.lang.String tagText) {
        boolean retVal = false;

        for (int t = 0; t < Tags.length; t++) {
            if (Tags[t].startsWith(tagText)) {
                try {
                    if (TagParm[t] != null) {
                        TagParm[t] = TagParm[t].trim();
                        if (TagParm[t].length() > 0) {
                            retVal = true;
                            break;
                        }
                    }
                } catch (Exception e) {
                    //System.out.println("isTAGset"+e);
                    retVal = false;
                    break;
                }
            }
        }
        return retVal;
    }

    public String getTagParameter(java.lang.String tagText) {
        String retVal = null;
        for (int t = 0; t < Tags.length; t++) {
            if (Tags[t].startsWith(tagText)) {
                try {
                    if (TagParm[t] != null) {
                        if (TagParm[t].length() >= 0) {
                            retVal = TagParm[t];
                            break;
                        }
                    }

                } catch (Exception e) {
                    //System.out.println("getTagParameter: NULL");
                    retVal = "";
                }
            }
        }
        return retVal;
    }

    public JTable populateTable(ResultSet rs, JTable tbl) {
        // Populate table with records returned
        Vector rows = new Vector();
        Vector colnames = new Vector();
        Vector field = new Vector();
        statusText("Populating result table");
        try {
            rs.last();
            int iTot = rs.getRow();
            prgBar.setStringPainted(true);
            prgBar.setMinimum(0);
            prgBar.setValue(0);
            prgBar.setMaximum(iTot);

            rs.beforeFirst();
            colnames.addElement("No");
            for (int c = 1; c <= rs.getMetaData().getColumnCount(); c++) {
                colnames.addElement(rs.getMetaData().getColumnName(c));
            }
            int iCurrent = 1;
            while (rs.next()) {
                field = new Vector();
                field.addElement("" + iCurrent);
                for (int f = 1; f <= rs.getMetaData().getColumnCount(); f++) {
                    field.addElement(rs.getString(f));
                }
                rows.addElement(field);
                if (iCurrent == 1000) {
                    break;
                }
                prgBar.setValue(iCurrent);
                if ((iCurrent % 20) == 0) {
                    this.paint(this.getGraphics());
                }
                iCurrent++;
            }
            prgBar.setValue(iTot);
        } catch (Exception e) {
            //System.out.println("populateTable" + e.getMessage());
        }
        tbl = new JTable(rows, colnames);

        return tbl;
    }

    public void setFixedWidth() {
        if (bNoOutput) {
            return;
        }
        tmpString = getTagParameter("--OUTPUTSTYLE=");
        if (tmpString != null) {
            if (tmpString.trim().equalsIgnoreCase("fw")) {
                tmpString = getTagParameter("--FWOFFSETS=");
                if (tmpString != null) {
                    sFixedWidthOffsets = tmpString.split(",");
                    if (sFixedWidthOffsets.length > 0) {
                        bFixedWidth = true;
                    }
                }
            }
        }
    }

    private void fileTidy() {
        String[] files = null;
        if (bNoOutput) {
            return;
        }
        try {

            tmpString = getTagParameter("--FILETIDY=");
            if (tmpString != null) {
                if (!tmpString.trim().equalsIgnoreCase("y")) {
                    return;
                }
            } else {
                return;
            }

            File inputFoldr = new File(filepath);
            files = inputFoldr.list();
            Arrays.sort(files);
            for (int i = 0; i < files.length; i++) {
                if (files[i].indexOf(fileExt) > 0) {
                    File file = new File(filepath + files[i]);
                    file.delete();
                    //if((file.canRead()) || (file.canWrite())){

                    //  }
                }
            }
            isfiletidy = true;
        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileTidy: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + " fileTidy: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    private void PriceRecCnt() {
        String[] files = null;
        if (bNoOutput) {
            return;
        }
        try {
            tmpString = getTagParameter("--PRICEROWCTCHK=");
            if (tmpString != null) {
                if (tmpString.trim().equalsIgnoreCase("y")) {
                    logflag = true;
                    return;
                }
            } else {
                logflag = false;
                return;
            }

        } catch (Exception e) {
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "PriceRecCnt: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "PriceRecCnt: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    private void assignAllSets() {

        //This function Reads all Parameters
        parseScript();
        Refresh();
        if (!bgotFilename) {
            buildFilename();
        }
        Refresh();
        setNoOutput();
        Refresh();
        setArchive();
        Refresh();
        //replaceDeclares();
        setFieldSeparator();
        Refresh();
        setFieldHeaders();
        Refresh();
        setShowNulls();
        Refresh();
        setForceTime();
        Refresh();
        setDataDateFormat();
        setDataTimeFormat();
        Refresh();
        setFixedWidth();
        Refresh();
        setCounterField();
        Refresh();
        PriceRecCnt();
    }

    private void disableControls() {

        btnRunNow.setEnabled(false);
        chkOpenOutput.setEnabled(false);
        chkPreview.setEnabled(false);
        chkQuery.setEnabled(false);
    }

    private void enableControls() {
        btnRunNow.setEnabled(true);
        chkOpenOutput.setEnabled(true);
        chkPreview.setEnabled(true);
        chkQuery.setEnabled(true);
    }

    private void Refresh() {
        pnlRefresh.paintImmediately(0, 0, pnlRefresh.getWidth(), pnlRefresh.getHeight());
    }

    public void writelogfile(String sfile, String svalue) {
        if (sfile != null && svalue != null) {
            try {
                BufferedWriter out = null;
                //File logFile = new File("O:\\Apps\\logs\\"+sToday +".txt");
                File logFile = new File(sfile);
                if (logFile.exists()) {
                    if (isfiletidy) {
                        out = new BufferedWriter(new FileWriter(sfile, false));
                    } else {
                        out = new BufferedWriter(new FileWriter(sfile, true));
                    }
                } else {
                    out = new BufferedWriter(new FileWriter(sfile, false));
                }
                out.write(svalue + "\r\n");
                out.close();
            } catch (IOException e) {
                //System.out.println("writelogfile"+e);
            }
        }

    }

    private void CompareCounts() {

        String tmStr = null;
        try {

            tmStr = "select LastLoadCnt,Description from prices.markets Where MarketCode='" + mrkCd + "';";
            rf = ExecuteSql(null, tmStr);
            while (rf.next()) {
                if (rf.getInt("LastLoadCnt") != totrec) {
                    writelogfile(LogFileName, "Price count discrepancy for " + mrkCd.toUpperCase() + "-" + rf.getInt("Description") + " Load Count= " + rf.getInt("LastLoadCnt") + " Output Count = " + totrec);
                    System.exit(1);
                }
            }
        } catch (Exception e) {
            //System.out.println("populateTable" + e.getMessage());
            if (!bAutoManRun) {
                JOptionPane.showMessageDialog(null, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "CompareCounts: " + e.getMessage());
                exitForm(null);
            } else {
                writelogfile(LogFileName, "!ERROR Exceuting! " + filepath + sFileNamePrefix + sFileExt + "CompareCounts: " + e.getMessage());
                System.exit(1);
            }
        }
    }

    private boolean CheckDir(String fname) {
        try {
            String parent = (new File(fname).getParent());
            System.out.println(parent);
            File dir = new File(parent);
            if (dir.exists() == false) {
                return dir.mkdir();
            } else {
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    private String Sreplace(String source, String pattern, String replace) {
        if (source != null) {
            final int len = pattern.length();
            StringBuffer sb = new StringBuffer();
            int found = -1;
            int start = 0;

            while ((found = source.toUpperCase().indexOf(pattern, start)) != -1) {
                sb.append(source.substring(start, found));
                sb.append(replace);
                start = found + len;
            }

            sb.append(source.substring(start));

            return sb.toString();
        } else {
            return "";
        }
    }
//    public void processSetTag(){
//        int st = 0;
//        int end = 0;
//        String script = jEditorPane1.getText();
//        String[] scriptLine = script.split("\n");
//        int setcount =0;
//        int i =0;
//        try{
//
//            for (int l=0; l< scriptLine.length; l++){
//                i = scriptLine[l].toLowerCase().indexOf("set @");
//                if (i>=0){
//                    setcount++;
//                }
//            }
//            // define array elements fron setcount
//            gSetVar = new String[setcount];
//            gSetValues = new String[setcount];
//
//            int v = 0;
//            for (int l=0; l< scriptLine.length; l++){
//                if (scriptLine[l].toLowerCase().indexOf("use")>=0){
//                    if (topdb==null){topdb = scriptLine[l].toLowerCase() + ";";}
//                }
//                if (scriptLine[l].toLowerCase().indexOf("set @")>=0){
//                    st = scriptLine[l].indexOf("@");
//                    end = scriptLine[l].indexOf(" =", st);
//                    gSetVar[v] = scriptLine[l].substring(st, end);
//                    System.out.println("use wca "+scriptLine[l].substring(end, scriptLine[l].length()).replaceFirst("=", ""));
//                    rf = ExecuteSql(topdb,scriptLine[l].substring(end, scriptLine[l].length()).replaceFirst("=", ""));
//                    rf.beforeFirst();
//                    while (rf.next()){
//                        gSetValues[v] = rf.getString(1).toString();
//                        //System.out.println( gSetValues[v]);
//                    }
//                    if (gSetValues[v]==null){
//                        if (!bAutoManRun){
//                            JOptionPane.showMessageDialog(null,"The set @ parameter returned error, possibly the database did not include in the select statement i.e From Database.dbo.table ","Error",2);
//                        }else{
//                            System.exit(6);
//                        }
//                    }
//                    gSetValues[v] = "'" + gSetValues[v] + "'"  ;
//                    v++;
//                }
//            }
//        } catch (Exception e) {
//            //System.out.println("processSetTag"+e);
//        }
//    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnRunNow;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox cboIP;
    private javax.swing.JComboBox cboSrvr;
    private javax.swing.JCheckBox chkOpenOutput;
    private javax.swing.JCheckBox chkPreview;
    private javax.swing.JCheckBox chkQuery;
    private javax.swing.JEditorPane jEditorPane1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JMenuItem mnuCloseScript;
    private javax.swing.JMenuItem mnuExit;
    private javax.swing.JMenuItem mnuOpenScript;
    private javax.swing.JMenuItem mnuSaveScript;
    private javax.swing.JMenuItem mnuSaveScriptAs;
    private javax.swing.JPanel pnlRefresh;
    private javax.swing.JProgressBar prgBar;
    private javax.swing.JTextField txtOutputPath;
    private javax.swing.JTextField txtScriptPath;
    private javax.swing.JLabel txtStatus;
    // End of variables declaration//GEN-END:variables
}
